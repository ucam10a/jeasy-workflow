/*
 * The MIT License
 *
 *  Copyright (c) 2020, Mahmoud Ben Hassine (mahmoud.benhassine@icloud.com)
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package org.jeasy.flows.workflow;

import java.util.UUID;

public abstract class AbstractWorkFlow implements WorkFlow {

    private String uuid;
    
    private String name;
    
    private AbstractWorkFlow nextFlow;
    
    private boolean isHumanFlow = false;

    AbstractWorkFlow(String name) {
        this.uuid = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public AbstractWorkFlow getNextFlow() {
        return nextFlow;
    }

    public void setNextFlow(AbstractWorkFlow nextFlow) {
        this.nextFlow = nextFlow;
    }

    public boolean isHumanFlow() {
        return isHumanFlow;
    }

    public void setHumanFlow(boolean isHumanFlow) {
        this.isHumanFlow = isHumanFlow;
    }

    public String getUuid() {
        return uuid;
    }

}
