/*
 * The MIT License
 *
 *  Copyright (c) 2020, Mahmoud Ben Hassine (mahmoud.benhassine@icloud.com)
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package org.jeasy.flows.workflow;

import org.jeasy.flows.work.CaseWork;
import org.jeasy.flows.work.NoOpWork;
import org.jeasy.flows.work.Work;
import org.jeasy.flows.work.WorkCaseReportPredicate;
import org.jeasy.flows.work.WorkContext;
import org.jeasy.flows.work.WorkReport;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A switch case flow is defined by 4 artifacts:
 *
 * <ul>
 *     <li>The work to execute first</li>
 *     <li>A predicate for the switch logic</li>
 *     <li>The work to execute if the predicate is satisfied with defined cases</li>
 *     <li>The work to execute if the predicate is not satisfied any case (optional)</li>
 * </ul>
 *
 * @see SwitchCaseFlow.Builder
 *
 * @author Yung-Long Li
 */
public class SwitchCaseFlow extends AbstractWorkFlow {

    private Work initialWorkUnit;
    private Work nextOnPredicateElse;
    private List<CaseWork> nextOnPredicateCaseList = new ArrayList<CaseWork>();
    private WorkCaseReportPredicate predicate;

    SwitchCaseFlow(String name, Work nextOnPredicateElse, WorkCaseReportPredicate predicate, CaseWork... nextOnPredicateCases) {
        super(name);
        this.initialWorkUnit = new NoOpWork();
        this.nextOnPredicateElse = nextOnPredicateElse;
        if (nextOnPredicateCases != null) {
            for (CaseWork predicateCase : nextOnPredicateCases) {
                nextOnPredicateCaseList.add(predicateCase);
            }
        }
        this.predicate = predicate;
    }

    /**
     * {@inheritDoc}
     */
    public WorkReport execute(WorkContext workContext) {
        WorkReport jobReport = initialWorkUnit.execute(workContext);
        int caseIdx = predicate.apply(jobReport);
        if (caseIdx >= 0) {
            Work nextOnPredicateWork = nextOnPredicateCaseList.get(caseIdx);
            jobReport = nextOnPredicateWork.execute(workContext);
        } else {
            if (nextOnPredicateElse != null && !(nextOnPredicateElse instanceof NoOpWork)) { // else is optional
                jobReport = nextOnPredicateElse.execute(workContext);
            }
        }
        return jobReport;
    }

    public static class Builder {

        private Builder() {
            // force usage of static method aNewConditionalFlow
        }

        public static NameStep aNewSwitchCaseFlow() {
            return new BuildSteps();
        }

        public interface NameStep extends ExecuteStep {
            ExecuteStep named(String name);
        }

        public interface ExecuteStep {
            WhenStep execute(Work nextOnPredicateElse, CaseWork... nextOnPredicateCases);
        }

        public interface WhenStep {
            ThenStep when(WorkCaseReportPredicate predicate);
        }

        public interface ThenStep {
            OtherwiseStep then(Work work);
        }

        public interface OtherwiseStep extends BuildStep {
            BuildStep otherwise(Work work);
        }

        public interface BuildStep {
            SwitchCaseFlow build();
        }

        private static class BuildSteps implements NameStep, ExecuteStep, WhenStep, ThenStep, OtherwiseStep, BuildStep {

            private String name;
            private Work nextOnPredicateElse;
            private WorkCaseReportPredicate predicate;
            private List<CaseWork> nextOnPredicateCaseList;

            BuildSteps() {
                this.name = UUID.randomUUID().toString();
                this.nextOnPredicateElse = new NoOpWork();
                this.nextOnPredicateCaseList = new ArrayList<CaseWork>();
                this.predicate = WorkCaseReportPredicate.ALWAYS_ELSE;
            }

            @Override
            public ExecuteStep named(String name) {
                this.name = name;
                return this;
            }

            @Override
            public WhenStep execute(Work nextOnPredicateElse, CaseWork... nextOnPredicateCases) {
                this.nextOnPredicateElse = nextOnPredicateElse;
                if (nextOnPredicateCases != null) {
                    for (CaseWork work : nextOnPredicateCases) {
                        this.nextOnPredicateCaseList.add(work);
                    }
                }
                return this;
            }

            @Override
            public ThenStep when(WorkCaseReportPredicate predicate) {
                this.predicate = predicate;
                return this;
            }

            @Override
            public OtherwiseStep then(Work work) {
                this.nextOnPredicateElse = work;
                return this;
            }

            @Override
            public BuildStep otherwise(Work work) {
                this.nextOnPredicateElse = work;
                return this;
            }

            @Override
            public SwitchCaseFlow build() {
                CaseWork[] nextOnPredicateCaseArr = new CaseWork[nextOnPredicateCaseList.size()];
                int idx = 0;
                for (CaseWork work : nextOnPredicateCaseList) {
                    nextOnPredicateCaseArr[idx] = work;
                    idx++;
                }
                return new SwitchCaseFlow(this.name, this.nextOnPredicateElse, this.predicate, nextOnPredicateCaseArr);
            }

        }
    }
}
