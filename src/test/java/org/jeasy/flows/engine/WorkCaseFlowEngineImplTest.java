/*
 * The MIT License
 *
 *  Copyright (c) 2020, Mahmoud Ben Hassine (mahmoud.benhassine@icloud.com)
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */
package org.jeasy.flows.engine;

import java.util.Map;
import java.util.Set;

import org.jeasy.flows.work.CaseWork;
import org.jeasy.flows.work.DefaultWorkReport;
import org.jeasy.flows.work.Work;
import org.jeasy.flows.work.WorkCaseReportPredicate;
import org.jeasy.flows.work.WorkContext;
import org.jeasy.flows.work.WorkReport;
import org.jeasy.flows.work.WorkStatus;
import org.jeasy.flows.workflow.*;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jeasy.flows.workflow.SwitchCaseFlow.Builder.aNewSwitchCaseFlow;
import static org.jeasy.flows.work.WorkReportPredicate.COMPLETED;
import static org.jeasy.flows.workflow.BasicFlow.Builder.aNewBasicFlow;
import static org.jeasy.flows.workflow.ConditionalFlow.Builder.aNewConditionalFlow;
import static org.jeasy.flows.workflow.RepeatFlow.Builder.aNewRepeatFlow;

public class WorkCaseFlowEngineImplTest {

    private final WorkFlowEngine workFlowEngine = new WorkFlowEngineImpl();

    @Test
    public void run() {
        // given
        WorkFlow workFlow = Mockito.mock(WorkFlow.class);
        WorkContext workContext = Mockito.mock(WorkContext.class);

        // when
        workFlowEngine.run(workFlow,workContext);

        // then
        Mockito.verify(workFlow).execute(workContext);
    }

    @Test
    public void defineWorkCaseFlowInlineAndExecuteIt() {

        PrintMessageCaseWork work0 = new PrintMessageCaseWork("start");
        PrintMessageCaseWork work1 = new PrintMessageCaseWork("foo");
        PrintMessageCaseWork work2 = new PrintMessageCaseWork("else case");
        PrintMessageCaseWork work3 = new PrintMessageCaseWork("true case");
        PrintMessageCaseWork work4 = new PrintMessageCaseWork("false case");
        PrintMessageCaseWork work5 = new PrintMessageCaseWork("case 1");
        PrintMessageCaseWork work6 = new PrintMessageCaseWork("case 2");
        PrintMessageCaseWork work7 = new PrintMessageCaseWork("done");
        
        // define work case report predicate
        WorkCaseReportPredicate casePredicate = new WorkCaseReportPredicate() {

            @Override
            public int apply(WorkReport workReport) {
                WorkContext context = workReport.getWorkContext();
                Object input = context.get("input");
                if (input == null) {
                    return -1;
                }
                return input.hashCode() % 2;
            }
            
        };
        
        AbstractWorkFlow finalFlow, repeatFlow, conditionalFlow, switchFlow;
        
        finalFlow = aNewBasicFlow()
                .execute(work7)
                .build();
        
        repeatFlow = aNewRepeatFlow()
            .named("print foo 3 times")
            .repeat(work1)
            .times(3)
            .build();
        
        conditionalFlow = aNewConditionalFlow()
                .execute(work0)
                .when(COMPLETED)
                .then(work3)
                .otherwise(work4)
                .build();
        repeatFlow.setNextFlow(conditionalFlow);
        work3.setNextFlow(finalFlow);
        
        switchFlow = aNewSwitchCaseFlow()
            .execute(work2 // else case
                     , work5, work6)
            .when(casePredicate)
            .then(null)
            .build();
        conditionalFlow.setNextFlow(switchFlow);
        work4.setNextFlow(switchFlow);
        
        ComplexWorkFlowEngineImpl workFlowEngine = new ComplexWorkFlowEngineImpl();
        WorkContext workContext = new WorkContext();
        WorkReport workReport = workFlowEngine.run(repeatFlow, workContext);
        assertThat(workReport.getStatus()).isEqualTo(WorkStatus.COMPLETED);
        System.out.println("workflow report = " + workReport);
    }

    static class PrintMessageWork implements Work {

        private String message;

        public PrintMessageWork(String message) {
            this.message = message;
        }

        public String getName() {
            return "print message work";
        }

        public WorkReport execute(WorkContext workContext) {
            System.out.println(message);
            return new DefaultWorkReport(WorkStatus.COMPLETED, workContext);
        }

    }
    
    static class PrintMessageCaseWork extends PrintMessageWork implements CaseWork {

        private AbstractWorkFlow nextFlow;

        public PrintMessageCaseWork(String message) {
            super(message);
        }
        
        public AbstractWorkFlow getNextFlow() {
            return nextFlow;
        }

        public void setNextFlow(AbstractWorkFlow nextFlow) {
            this.nextFlow = nextFlow;
        }

    }
    
    static class WordCountWork implements Work {

        private int partition;

        public WordCountWork(int partition) {
            this.partition = partition;
        }

        @Override
        public String getName() {
            return "count words in a given string";
        }

        @Override
        public WorkReport execute(WorkContext workContext) {
            String input = (String) workContext.get("partition" + partition);
            workContext.put("wordCountInPartition" + partition, input.split(" ").length);
            return new DefaultWorkReport(WorkStatus.COMPLETED, workContext);
        }
    }
    
    static class AggregateWordCountsWork implements Work {

        @Override
        public String getName() {
            return "aggregate word counts from partitions";
        }

        @Override
        public WorkReport execute(WorkContext workContext) {
            Set<Map.Entry<String, Object>> entrySet = workContext.getEntrySet();
            int sum = 0;
            for (Map.Entry<String, Object> entry : entrySet) {
                if (entry.getKey().contains("InPartition")) {
                    sum += (int) entry.getValue();
                }
            }
            workContext.put("totalCount", sum);
            return new DefaultWorkReport(WorkStatus.COMPLETED, workContext);
        }
    }

    static class PrintWordCount implements Work {

        @Override
        public String getName() {
            return "print total word count";
        }

        @Override
        public WorkReport execute(WorkContext workContext) {
            int totalCount = (int) workContext.get("totalCount");
            System.out.println(totalCount);
            return new DefaultWorkReport(WorkStatus.COMPLETED, workContext);
        }
    }
}
